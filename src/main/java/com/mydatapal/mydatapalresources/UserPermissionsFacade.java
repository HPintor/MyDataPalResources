/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mydatapal.mydatapalresources;

import entities.UserPermissions;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author hpintor
 */
@Stateless
public class UserPermissionsFacade extends AbstractFacade<UserPermissions> {

    @PersistenceContext(unitName = "com.mydatapal_MyDataPalResources_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public UserPermissionsFacade() {
        super(UserPermissions.class);
    }
    
}
