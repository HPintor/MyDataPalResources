/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hpintor
 */
@Entity
@Table(name = "session_documents")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SessionDocuments.findAll", query = "SELECT s FROM SessionDocuments s"),
    @NamedQuery(name = "SessionDocuments.findByIdDoc", query = "SELECT s FROM SessionDocuments s WHERE s.idDoc = :idDoc"),
    @NamedQuery(name = "SessionDocuments.findByDescription", query = "SELECT s FROM SessionDocuments s WHERE s.description = :description"),
    @NamedQuery(name = "SessionDocuments.findByName", query = "SELECT s FROM SessionDocuments s WHERE s.name = :name"),
    @NamedQuery(name = "SessionDocuments.findByPath", query = "SELECT s FROM SessionDocuments s WHERE s.path = :path"),
    @NamedQuery(name = "SessionDocuments.findBySession", query = "SELECT s FROM SessionDocuments s WHERE s.idSession = :idSession")})
public class SessionDocuments implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_doc")
    private Integer idDoc;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "path")
    private String path;
    @JoinColumn(name = "id_session", referencedColumnName = "id")
    @ManyToOne
    private Session idSession;

    public SessionDocuments() {
    }

    public SessionDocuments(Integer idDoc) {
        this.idDoc = idDoc;
    }

    public Integer getIdDoc() {
        return idDoc;
    }

    public void setIdDoc(Integer idDoc) {
        this.idDoc = idDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Session getIdSession() {
        return idSession;
    }

    public void setIdSession(Session idSession) {
        this.idSession = idSession;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDoc != null ? idDoc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SessionDocuments)) {
            return false;
        }
        SessionDocuments other = (SessionDocuments) object;
        if ((this.idDoc == null && other.idDoc != null) || (this.idDoc != null && !this.idDoc.equals(other.idDoc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "{\"idDoc\":"+idDoc+",\"idSession\":"+idSession.getId()+",\"name\":\""+name+"\",\"description\":\""+description+"\"}";
    }
    
}
