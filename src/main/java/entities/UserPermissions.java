/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hpintor
 */
@Entity
@Table(name = "user_permissions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserPermissions.findAll", query = "SELECT u FROM UserPermissions u"),
    @NamedQuery(name = "UserPermissions.findByAdmin", query = "SELECT u FROM UserPermissions u WHERE u.admin = :admin"),
    @NamedQuery(name = "UserPermissions.findByReading", query = "SELECT u FROM UserPermissions u WHERE u.reading = :reading"),
    @NamedQuery(name = "UserPermissions.findByWriting", query = "SELECT u FROM UserPermissions u WHERE u.writing = :writing"),
    @NamedQuery(name = "UserPermissions.findByIdUser", query = "SELECT u FROM UserPermissions u WHERE u.userPermissionsPK.idUser = :idUser"),
    @NamedQuery(name = "UserPermissions.findByIdStudy", query = "SELECT u FROM UserPermissions u WHERE u.userPermissionsPK.idStudy = :idStudy")})
public class UserPermissions implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserPermissionsPK userPermissionsPK;
    @Column(name = "admin")
    private Boolean admin;
    @Column(name = "reading")
    private Boolean reading;
    @Column(name = "writing")
    private Boolean writing;

    public UserPermissions() {
    }

    public UserPermissions(UserPermissionsPK userPermissionsPK) {
        this.userPermissionsPK = userPermissionsPK;
    }

    public UserPermissions(int idUser, int idStudy) {
        this.userPermissionsPK = new UserPermissionsPK(idUser, idStudy);
    }

    public UserPermissionsPK getUserPermissionsPK() {
        return userPermissionsPK;
    }

    public void setUserPermissionsPK(UserPermissionsPK userPermissionsPK) {
        this.userPermissionsPK = userPermissionsPK;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public Boolean getReading() {
        return reading;
    }

    public void setReading(Boolean reading) {
        this.reading = reading;
    }

    public Boolean getWriting() {
        return writing;
    }

    public void setWriting(Boolean writing) {
        this.writing = writing;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userPermissionsPK != null ? userPermissionsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserPermissions)) {
            return false;
        }
        UserPermissions other = (UserPermissions) object;
        if ((this.userPermissionsPK == null && other.userPermissionsPK != null) || (this.userPermissionsPK != null && !this.userPermissionsPK.equals(other.userPermissionsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserPermissions[ userPermissionsPK=" + userPermissionsPK + " ]";
    }
    
}
