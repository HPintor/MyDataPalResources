/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author hpintor
 */
@Embeddable
public class UserPermissionsPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id_user")
    private int idUser;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_study")
    private int idStudy;

    public UserPermissionsPK() {
    }

    public UserPermissionsPK(int idUser, int idStudy) {
        this.idUser = idUser;
        this.idStudy = idStudy;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdStudy() {
        return idStudy;
    }

    public void setIdStudy(int idStudy) {
        this.idStudy = idStudy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) idUser;
        hash += (int) idStudy;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserPermissionsPK)) {
            return false;
        }
        UserPermissionsPK other = (UserPermissionsPK) object;
        if (this.idUser != other.idUser) {
            return false;
        }
        if (this.idStudy != other.idStudy) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.UserPermissionsPK[ idUser=" + idUser + ", idStudy=" + idStudy + " ]";
    }
    
}
