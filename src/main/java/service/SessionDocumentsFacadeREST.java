/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.dropbox.core.DbxException;
import com.dropbox.core.DbxRequestConfig;
import com.dropbox.core.NetworkIOException;
import com.dropbox.core.RetryException;
import entities.SessionDocuments;
import java.io.InputStream;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import com.dropbox.core.v2.*;
import com.dropbox.core.v2.files.CommitInfo;
import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.UploadErrorException;
import com.dropbox.core.v2.files.UploadSessionCursor;
import com.dropbox.core.v2.files.UploadSessionFinishErrorException;
import com.dropbox.core.v2.files.UploadSessionLookupErrorException;
import com.dropbox.core.v2.files.WriteMode;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import com.dropbox.core.*;
import com.dropbox.core.v1.*;
import com.mydatapal.mydatapalresources.SessionDocumentsFacade;
import com.mydatapal.mydatapalresources.SessionFacade;
import entities.Session;
import entities.Study;

import java.io.*;
import java.util.Locale;
import javax.ejb.EJB;
import javax.ws.rs.core.Response;

/**
 *
 * @author hpintor
 */
@Stateless
@Path("entities.sessiondocuments")
public class SessionDocumentsFacadeREST extends AbstractFacade<SessionDocuments> {

    @EJB
    SessionFacade facade = new SessionFacade();
    @EJB
    SessionDocumentsFacade sdf = new SessionDocumentsFacade();

    final static private String APP_KEY = "6t186ujlcxa9db5";

    final static private String APP_SECRET = "c56x8uzvz7irtta";

    private DbxAppInfo appInfo;
    private DbxRequestConfig requestConfig = new DbxRequestConfig("XLUgqErWxTcAAAAAAAAAzMa9hKJgD2lIm7sHpe8r8TlE8ClBJ-5hg36JtQvijnGe");
    private DbxClientV2 dbClient = new DbxClientV2(requestConfig, "XLUgqErWxTcAAAAAAAAAzMa9hKJgD2lIm7sHpe8r8TlE8ClBJ-5hg36JtQvijnGe");
    private DbxWebAuthNoRedirect webAuth;
    private String accessToken = "XLUgqErWxTcAAAAAAAAAzMa9hKJgD2lIm7sHpe8r8TlE8ClBJ-5hg36JtQvijnGe";
    private DbxClientV1 client;
    private String pathDestiny = null;

    @Context
    private ServletContext app;
    @PersistenceContext(unitName = "com.mydatapal_MyDataPalResources_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public SessionDocumentsFacadeREST() {
        super(SessionDocuments.class);
        appInfo = new DbxAppInfo(APP_KEY, APP_SECRET);
        webAuth = new DbxWebAuthNoRedirect(requestConfig, appInfo);
        client = new DbxClientV1(requestConfig, accessToken);
    }

    @POST
    @Path("/upload/{sessionID}")
    //@Produces({MediaType.APPLICATION_JSON})
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void upload(FormDataMultiPart form, @PathParam("sessionID") Integer sessionID) throws IOException, DbxException {
        pathDestiny = "/" + sessionID;

        System.out.println("Entering upload...");
        FormDataBodyPart uploadedFile = form.getField("file");
        ContentDisposition headerOfFile = uploadedFile.getContentDisposition();
        File file = uploadedFile.getValueAs(File.class);
        String fileName = uploadedFile.getContentDisposition().getFileName();

        SessionDocuments sd = new SessionDocuments();
        Session s = facade.find(sessionID);

        sd.setIdSession(s);
        sd.setIdDoc(1);
        sd.setName(fileName);
        System.out.println("CRLLLLLLL________________>>>>>"+sd);
        sdf.create(sd);
        
        chunkedUploadFile(dbClient, file, pathDestiny + "/" + fileName);
        System.out.println("Exiting upload --> ");// + file.getCanonicalPath());

    }

    private static final long CHUNKED_UPLOAD_CHUNK_SIZE = 8L << 20; // 8MiB
    private static final int CHUNKED_UPLOAD_MAX_ATTEMPTS = 5;

    private static void uploadFile(DbxClientV2 dbxClient, File localFile, String dropboxPath) {
        try (InputStream in = new FileInputStream(localFile)) {
            FileMetadata metadata = dbxClient.files().uploadBuilder(dropboxPath)
                    .withMode(WriteMode.ADD)
                    .withClientModified(new Date(localFile.lastModified()))
                    .uploadAndFinish(in);

            System.out.println(metadata.toStringMultiline());
        } catch (UploadErrorException ex) {
            System.err.println("Error uploading to Dropbox: " + ex.getMessage());

        } catch (DbxException ex) {
            System.err.println("Error uploading to Dropbox: " + ex.getMessage());

        } catch (IOException ex) {
            System.err.println("Error reading from file \"" + localFile + "\": " + ex.getMessage());

        }
    }

    private static void chunkedUploadFile(DbxClientV2 dbxClient, File localFile, String dropboxPath) throws IOException {
        long size = localFile.length();

        // assert our file is at least the chunk upload size. We make this assumption in the code
        // below to simplify the logic.
        if (size < CHUNKED_UPLOAD_CHUNK_SIZE) {
            System.err.println("File too small, use upload() instead.");
            uploadFile(dbxClient, localFile, dropboxPath);

            return;
        }

        long uploaded = 0L;
        DbxException thrown = null;

        // Chunked uploads have 3 phases, each of which can accept uploaded bytes:
        //
        //    (1)  Start: initiate the upload and get an upload session ID
        //    (2) Append: upload chunks of the file to append to our session
        //    (3) Finish: commit the upload and close the session
        //
        // We track how many bytes we uploaded to determine which phase we should be in.
        String sessionId = null;
        for (int i = 0; i < CHUNKED_UPLOAD_MAX_ATTEMPTS; ++i) {
            if (i > 0) {
                System.out.printf("Retrying chunked upload (%d / %d attempts)\n", i + 1, CHUNKED_UPLOAD_MAX_ATTEMPTS);
            }

            try (InputStream in = new FileInputStream(localFile)) {
                // if this is a retry, make sure seek to the correct offset
                in.skip(uploaded);

                // (1) Start
                if (sessionId == null) {
                    sessionId = dbxClient.files().uploadSessionStart()
                            .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE)
                            .getSessionId();
                    uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
                    printProgress(uploaded, size);
                }

                UploadSessionCursor cursor = new UploadSessionCursor(sessionId, uploaded);

                // (2) Append
                while ((size - uploaded) > CHUNKED_UPLOAD_CHUNK_SIZE) {
                    dbxClient.files().uploadSessionAppendV2(cursor)
                            .uploadAndFinish(in, CHUNKED_UPLOAD_CHUNK_SIZE);
                    uploaded += CHUNKED_UPLOAD_CHUNK_SIZE;
                    printProgress(uploaded, size);
                    cursor = new UploadSessionCursor(sessionId, uploaded);
                }

                // (3) Finish
                long remaining = size - uploaded;
                CommitInfo commitInfo = CommitInfo.newBuilder(dropboxPath)
                        .withMode(WriteMode.ADD)
                        .withClientModified(new Date(localFile.lastModified()))
                        .build();
                FileMetadata metadata = dbxClient.files().uploadSessionFinish(cursor, commitInfo)
                        .uploadAndFinish(in, remaining);

                System.out.println(metadata.toStringMultiline());
                return;
            } catch (RetryException ex) {
                thrown = ex;
                // RetryExceptions are never automatically retried by the client for uploads. Must
                // catch this exception even if DbxRequestConfig.getMaxRetries() > 0.
                sleepQuietly(ex.getBackoffMillis());
                continue;
            } catch (NetworkIOException ex) {
                thrown = ex;
                // network issue with Dropbox (maybe a timeout?) try again
                continue;
            } catch (UploadSessionLookupErrorException ex) {
                if (ex.errorValue.isIncorrectOffset()) {
                    thrown = ex;
                    // server offset into the stream doesn't match our offset (uploaded). Seek to
                    // the expected offset according to the server and try again.
                    uploaded = ex.errorValue
                            .getIncorrectOffsetValue()
                            .getCorrectOffset();
                    continue;
                } else {
                    // Some other error occurred, give up.
                    System.err.println("Error uploading to Dropbox: " + ex.getMessage());
                    System.exit(1);
                    return;
                }
            } catch (UploadSessionFinishErrorException ex) {
                if (ex.errorValue.isLookupFailed() && ex.errorValue.getLookupFailedValue().isIncorrectOffset()) {
                    thrown = ex;
                    // server offset into the stream doesn't match our offset (uploaded). Seek to
                    // the expected offset according to the server and try again.
                    uploaded = ex.errorValue
                            .getLookupFailedValue()
                            .getIncorrectOffsetValue()
                            .getCorrectOffset();
                    continue;
                } else {
                    // some other error occurred, give up.
                    System.err.println("Error uploading to Dropbox: " + ex.getMessage());

                    return;
                }
            } catch (DbxException ex) {
                System.err.println("Error uploading to Dropbox: " + ex.getMessage());

                return;
            } catch (IOException ex) {
                System.err.println("Error reading from file \"" + localFile + "\": " + ex.getMessage());
                return;
            }
        }
    }

    private static void printProgress(long uploaded, long size) {
        System.out.printf("Uploaded %12d / %12d bytes (%5.2f%%)\n", uploaded, size, 100 * (uploaded / (double) size));
    }

    private static void sleepQuietly(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException ex) {
            // just exit
            System.err.println("Error uploading to Dropbox: interrupted during backoff.");

        }
    }
    
    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("findBySession/{idSession}")
    public Response findBySession(@PathParam("idSession") Integer idSession) {
        Session s = facade.find(idSession);
        List<SessionDocuments> s2 = em.createNamedQuery("SessionDocuments.findBySession")
                .setParameter("idSession", s)
                .getResultList();
        if (s2 == null) {
            return Response.status(404).entity("{\"State\":\"ERROR\",\"Description\":\"Nao encontrado\"}").build();
        } else {
            return Response.status(201).entity(s2.toString()).build();
        }
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_JSON})
    public void create(SessionDocuments entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, SessionDocuments entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public SessionDocuments find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Path("/download/{sessionID}/{fileName}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.MULTIPART_FORM_DATA)
    public File download(@PathParam("sessionID") Integer sessionID, @PathParam("fileName") String fileName) throws FileNotFoundException, DbxException, IOException {

        //Download File
        FileOutputStream outputStream = new FileOutputStream(fileName);

        if (sessionID == null) {
            DbxEntry.File downloadedFile = client.getFile("/" + fileName, null, outputStream);

        } else {
            DbxEntry.File downloadedFile = client.getFile("/" + sessionID + "/" + fileName, null, outputStream);

        }
        outputStream.close();

        //Save File your own computer
        File file = new File(fileName);

        return file;

    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<SessionDocuments> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<SessionDocuments> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}