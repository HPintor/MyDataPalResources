/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.google.gson.Gson;
import com.mydatapal.mydatapalresources.SessionFacade;
import com.mydatapal.mydatapalresources.StudyFacade;
import com.mydatapal.mydatapalresources.UserPermissionsFacade;
import entities.Session;
import entities.Study;
import entities.UserPermissions;
import entities.UserPermissionsPK;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author hpintor
 */
@Stateless
@Path("entities.session")
public class SessionFacadeREST extends AbstractFacade<Session> {

    @EJB
    StudyFacade facade = new StudyFacade();
    @EJB
    UserPermissionsFacade facade2 = new UserPermissionsFacade();
    @EJB
    SessionFacade facade3 = new SessionFacade();

    @PersistenceContext(unitName = "com.mydatapal_MyDataPalResources_war_1.0-SNAPSHOTPU")
    private EntityManager em;

    public SessionFacadeREST() {
        super(Session.class);
    }

    @GET
    @Path("create/{idUser}/{idStudy}/{name}/{description}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response create(@PathParam("idUser") Integer idUser, @PathParam("idStudy") Integer idStudy, @PathParam("name") String name, @PathParam("description") String description) {
        Study st = facade.find(idStudy);
        UserPermissions up = facade2.find(new UserPermissionsPK(idUser, idStudy));
        if (st == null) {
            return Response.status(404).entity("{\"State\":\"ERROR\",\"Description\":\"Nao encontrado\"}").build();
        } else if (up.getWriting() && up != null) {
            Session s = new Session();
            s.setId(1);
            s.setIdStudy(st);
            s.setDescription(description);
            s.setName(name);
            facade3.create(s);
            return Response.status(201).entity(s).build();
        }
        return Response.status(401).entity("{\"State\":\"ERROR\",\"Description\":\"Permissão negada\"}").build();

    }

    @GET
    @Path("edit/{idUser}/{idSession}/{name}/{description}")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response edit(@PathParam("idUser") Integer idUser, @PathParam("idSession") Integer idSession, @PathParam("name") String name, @PathParam("description") String description) {
        Session s = facade3.find(idSession);
        Study study = s.getIdStudy();
        int idStudy = study.getId();
        UserPermissions up = facade2.find(new UserPermissionsPK(idUser, idStudy));
        if (s == null) {
            return Response.status(404).entity("{\"State\":\"ERROR\",\"Description\":\"Nao encontrado\"}").build();
        } else if (up.getWriting() && up != null) {
            s.setDescription(description);
            s.setName(name);
            facade3.edit(s);
            return Response.status(201).entity(s).build();
        } else {
            return Response.status(401).entity("{\"State\":\"ERROR\",\"Description\":\"Permissão negada\"}").build();
        }

    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("deleteSession/{id}/{idUser}")
    public Response delete(@PathParam("id") Integer id, @PathParam("idUser") Integer idUser) {
        Session s = facade3.find(id);
        UserPermissions up = facade2.find(new UserPermissionsPK( idUser,s.getIdStudy().getId()));
        if (s == null) {
            return Response.status(404).entity("{\"State\":\"ERROR\",\"Description\":\"Nao encontrado\"}").build();
        } else if (up.getAdmin()
                && up != null) {
            super.remove(s);
            return Response.status(201).entity("{\"State\":\"OK}").build();
        } else {
            return Response.status(1).entity("{\"State\":\"ERROR\",\"Description\":\"Permissão Negada\"}").build();
        }
    }

    @GET
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    @Path("findByStudy/{idStudy}")
    public Response findByStudy(@PathParam("idStudy") Integer idStudy) {
        Study st = facade.find(idStudy);
        List<Session> s2 = em.createNamedQuery("Session.findByStudy")
                .setParameter("idStudy", st)
                .getResultList();
                
        if (s2 == null) {
            return Response.status(404).entity("{\"State\":\"ERROR\",\"Description\":\"Nao encontrado\"}").build();
        } else {
            return Response.status(201).entity(s2.toString()).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Session find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Session> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Session> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
