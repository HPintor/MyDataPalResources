package entities;

import entities.Session;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-19T20:45:20")
@StaticMetamodel(SessionDocuments.class)
public class SessionDocuments_ { 

    public static volatile SingularAttribute<SessionDocuments, Integer> idDoc;
    public static volatile SingularAttribute<SessionDocuments, String> description;
    public static volatile SingularAttribute<SessionDocuments, Session> idSession;
    public static volatile SingularAttribute<SessionDocuments, String> name;
    public static volatile SingularAttribute<SessionDocuments, String> path;

}