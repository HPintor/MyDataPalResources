package entities;

import entities.SessionDocuments;
import entities.Study;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-19T20:45:20")
@StaticMetamodel(Session.class)
public class Session_ { 

    public static volatile SingularAttribute<Session, Integer> id;
    public static volatile SingularAttribute<Session, String> description;
    public static volatile SingularAttribute<Session, String> name;
    public static volatile SingularAttribute<Session, Study> idStudy;
    public static volatile CollectionAttribute<Session, SessionDocuments> sessionDocumentsCollection;

}