package entities;

import entities.Session;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-19T20:45:20")
@StaticMetamodel(Study.class)
public class Study_ { 

    public static volatile SingularAttribute<Study, Integer> id;
    public static volatile SingularAttribute<Study, String> description;
    public static volatile SingularAttribute<Study, String> name;
    public static volatile CollectionAttribute<Study, Session> sessionCollection;

}