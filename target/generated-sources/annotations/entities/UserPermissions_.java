package entities;

import entities.UserPermissionsPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-12-19T20:45:20")
@StaticMetamodel(UserPermissions.class)
public class UserPermissions_ { 

    public static volatile SingularAttribute<UserPermissions, Boolean> reading;
    public static volatile SingularAttribute<UserPermissions, Boolean> admin;
    public static volatile SingularAttribute<UserPermissions, Boolean> writing;
    public static volatile SingularAttribute<UserPermissions, UserPermissionsPK> userPermissionsPK;

}